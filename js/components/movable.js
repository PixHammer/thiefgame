/**
 * Component description
 * @moduleName Movable
 */
bento.define('components/movable', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/entity',
    'bento/eventsystem',
    'bento/utils',
    'bento/tween'
], function (
    Bento,
    Vector2,
    Rectangle,
    Entity,
    EventSystem,
    Utils,
    Tween
) {
    'use strict';
    /*
        TODO: Descibe this
    */
    return function (settings) {
        var viewport = Bento.getViewport();
        var entity;
        var component = {
            name: 'movable',
            // PROPERTIES
            gravity: settings.gravity || 0,
            friction: settings.friction || new Vector2(0, 1),
            velocity: new Vector2(0, 0),
            velocityDecimal: new Vector2(0, 0),
            velocityInteger: new Vector2(0, 0),
            performFriction: true,
            slopeMaxStep: 8,
            // FUNCTIONS
            update: function (data) {
                // - get references
                var e = this.parent;
                var tc = Bento.objects.get('tileCollider');

                // - accelerate
                this.velocity.y += this.gravity;

                // - move & collide
                // convert decimal velocity to integer
                this.velocityDecimal.addTo(this.velocity);
                this.velocityInteger = new Vector2(Math.round(this.velocityDecimal.x), Math.round(this.velocityDecimal.y));
                this.velocityDecimal.subtractFrom(this.velocityInteger);

                // x
                this.moveX(this.velocityInteger.x,true,function(){
                    component.velocity.x = 0;
                });
                // y
                this.moveY(this.velocityInteger.y,function(){
                    component.velocity.y = 0;
                });

                // - friction
                if (this.performFriction) {
                    this.velocity.multiplyWith(this.friction);
                }
            },
            // move on the x axis
            moveX: function (dx,doSlope,collisionCallback) {
                var e = this.parent;
                var tc = Bento.objects.get('tileCollider');
                var delta = dx;
                var slopeDeltaY = 0;
                var onGround = this.checkSolidRelative(new Vector2(0, 1));

                if (doSlope) {
                    slopeDeltaY = this.moveY(-this.slopeMaxStep,function(){});
                }
                if (Math.abs(dx) !== 0) {
                    //step
                    e.position.x += dx;
                    //if we collide, move out and stop
                    if ( tc.collideRect(e.getBoundingBox()) ) {
                        while ( tc.collideRect(e.getBoundingBox()) ) {
                            e.position.x -= Utils.sign(dx);
                            delta -= Utils.sign(dx);
                        }
                        collisionCallback();
                    }
                }
                if (slopeDeltaY !== 0) {
                    this.moveY(-slopeDeltaY,function(){});
                }
                if (doSlope && onGround && !this.checkSolidRelative(new Vector2(0, 1)) && this.checkSolidRelative(new Vector2(0, this.slopeMaxStep))) {
                    this.moveY(this.slopeMaxStep,function(){});
                }
                return delta;
            },
            //move on the y axis
            moveY: function (dy,collisionCallback) {
                var e = this.parent;
                var tc = Bento.objects.get('tileCollider');
                var delta = dy;

                if (Math.abs(dy) !== 0) {
                    //step
                    e.position.y += dy;
                    //if we collide, move out and stop
                    if ( tc.collideRect(e.getBoundingBox()) ) {
                        while ( tc.collideRect(e.getBoundingBox()) ) {
                            e.position.y -= Utils.sign(dy);
                            delta -= Utils.sign(dy);
                        }
                        collisionCallback();
                    }
                }
                return delta;
            },
            //check a position relative to hte current player position if a collision is there
            checkSolidRelative: function (relativePosition) {
                var tc = Bento.objects.get('tileCollider');
                var e = this.parent;
                return tc.collideRect( e.getBoundingBox().offset(relativePosition.clone())) ;
            }
        };
        return component;
    };
});