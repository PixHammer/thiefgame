/**
 * Component description
 * @moduleName InputHandler
 */
 bento.define('components/inputhandler', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/entity',
    'bento/eventsystem',
    'bento/utils',
    'bento/tween',
    'bento/components/clickable',
], function (
    Bento,
    Vector2,
    Rectangle,
    Entity,
    EventSystem,
    Utils,
    Tween,
    Clickable
) {
    'use strict';
    /*

     */
    return function (settings) {
        var viewport = Bento.getViewport();
        var entity;

        // GUI
        var imageJoyStick = {
            image: Bento.assets.getImage('GUI/joypad'),
            width: 48,
            height: 48
        };
        var joystickGUI = new Entity({
            z: 1000,
            name: 'joystick',
            position: new Vector2(0, 0),
            float:true,
            components: [
                {
                    draw: function (data) {
                        if (inputHandler.mouseDown) {
                            Utils.renderImage(data,imageJoyStick,0,inputHandler.joystickOrigin,0);
                            Utils.renderImage(data,imageJoyStick,1,inputHandler.joystickPosition,0);
                        }
                    }
                }
            ]
        });

        // collects raw input and injects it into the handler's properties
        var clickable = new Clickable({
            pointerDown: function (data) {
                 inputHandler.mouseDown = true;
                 inputHandler.mousePos = data.position.clone();
                 inputHandler.joystickOrigin = data.position.clone();
                 inputHandler.joystickPosition = data.position.clone();
            },
            pointerUp: function (data) {
                inputHandler.mouseDown = false;
            },
            pointerMove: function (data) {
                inputHandler.mousePos = data.position.clone();
                inputHandler.joystickPosition = data.position.clone();
            }
        });

        // wrangles the raw input into a tangible form (swipes, positions, down booleans, 'joystick')
        var inputHandler = {
            // --- PROPERTIES --- //
            name: 'inputHandler',
            // MOUSE
            mouseDown: false,
            mouseDownLast: false,
            mousePos: new Vector2(0, 0),
            mousePosLast: new Vector2(0, 0),
            // SWIPES
            doSwipe: settings.doSwipe,
            swipeSamples: [],
            swipe: new Vector2(0, 0),
            // JOYSTICKS
            doJoystick: settings.doJoystick,
            joystickOrigin: new Vector2(0, 0),
            joystickPosition: new Vector2(0, 0),
            joystickDeadZone: settings.joystickDeadZone || 0.125,
            joystickMaxDist: settings.joystickMaxDist || 24,
            joystick: new Vector2(0, 0),
            // --- FUNCTIONS --- //
            attached: function (data) {
        		entity = data.entity;
        		entity.attach(clickable);
                Bento.objects.attach(joystickGUI);
        		EventSystem.on('postUpdate', postUpdate);
            },
            destroy: function (data) {
                entity.remove(clickable);
                Bento.objects.remove(joystickGUI);
                EventSystem.off('postUpdate', postUpdate);
            },
            start: function (data) {
                // set defaults
                if (!Utils.isDefined(this.doSwipe)) {
                    this.doSwipe = true;
                }
                if (!Utils.isDefined(this.doJoystick)) {
                    this.doJoystick = true;
                }
            },
            update: function (data) {
                var currentDifference = this.mousePos.subtract(this.mousePosLast);

                // swiping stuff
                if (this.doSwipe) {
                    // sample differences for the past 5 frames
                    this.swipeSamples.push(currentDifference);
                    if (this.swipeSamples.length > 2) {
                        this.swipeSamples.splice(0,1);
                    }
                    // derive 'swipe' from the average of the last 5 frames of difference
                    this.swipe = new Vector2(0, 0);
                    for (var i = 0; i < this.swipeSamples.length; i++) {
                        this.swipe.addTo(this.swipeSamples[i]);
                    }
                    this.swipe.scalarMultiplyWith(1/this.swipeSamples.length);
                }

                // joystick stuff
                if (this.doJoystick && this.mouseDown) {
                    var joystickDifference = this.joystickPosition.subtract(this.joystickOrigin);
                    var joystickDirection = joystickDifference.clone().normalize();
                    var joystickDistance = joystickDifference.clone().magnitude();
                    if (joystickDistance > this.joystickMaxDist) {
                        this.joystickOrigin = this.joystickPosition.add(joystickDirection.scalarMultiply(-this.joystickMaxDist));
                    }
                    if (joystickDistance/this.joystickMaxDist > this.joystickDeadZone) {
                        this.joystick = joystickDirection.scalarMultiply(joystickDistance/this.joystickMaxDist);
                    } else {
                        this.joystick = new Vector2(0, 0);
                    }
                } else {
                    this.joystick = new Vector2(0, 0);
                }

                //keyboard overides
                var input = Bento.input;
                if (input.isKeyDown('left')){
                    this.joystick.x--;
                }
                if (input.isKeyDown('right')){
                    this.joystick.x++;
                }
                if (input.isKeyDown('up')){
                    this.joystick.y--;
                }
                if (input.isKeyDown('down')){
                    this.joystick.y++;
                }
            }
        };
        var postUpdate = function (data) {
            if (inputHandler) {
                inputHandler.mousePosLast = inputHandler.mousePos.clone();
                inputHandler.mouseDownLast = inputHandler.mouseDown;
            }
        };

        return inputHandler;
    };
});