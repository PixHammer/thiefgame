/**
 * Extends utils with more useful functions.
 * See bento/utils
 */
bento.define('utils', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/components/sprite',
    'bento/components/clickable',
    'bento/entity',
    'bento/utils',
    'bento/tween'
], function (
    Bento,
    Vector2,
    Rectangle,
    Sprite,
    Clickable,
    Entity,
    Utils,
    Tween
) {
    'use strict';
    var utils = {
        // --- DATA --- //
        // layers
        layers: {
            CAMERA: -1,
            BACKGROUND: 0,
            PLAYER: 1,
            GUI: 2
        },
        // clone an object
        cloneObject: function(obj) {
            if (null == obj || "object" != typeof obj) return obj;
            var copy = obj.constructor();
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
            }
            return copy;
        },
        // run through the contents of an object
        forEachInObject: function (object,doFunction){
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    var element = object[key];
                    doFunction(element, key, object);
                }
            }
        },
        // --- TIMING --- //
        // acts as a timeout 
        timeout: function (time, callback) {
            var tween = new Tween({
                from: 0,
                to: 1,
                in: time,
                ease: 'linear',
                onComplete: callback
            });
            return tween;
        },
        // completely locks the game for X milliseconds (needs mobile testing)
        sleep: function (milliseconds) {
            var endTime = new Date().getTime() + milliseconds;
            while (new Date().getTime() < endTime) {}
        },
        // --- RENDERING --- //
        // renders a packed image
        renderImage: function (data,image,frame,atPos,rotation) {
            data.renderer.save();
            data.renderer.translate(atPos.x,atPos.y);
            data.renderer.rotate(rotation);
            data.renderer.drawImage(image.image, image.width*frame, 0, image.width, image.height, -image.width/2, -image.height/2, image.width, image.height);
            data.renderer.restore();
        }
    };
    Utils.extend(Utils, utils);
    return Utils;
});