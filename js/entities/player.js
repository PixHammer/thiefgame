/**
 * The Player with ball bouncing behaviour
 * @moduleName Player
 */
bento.define('entities/player', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/components/sprite',
    'bento/components/clickable',
    'bento/entity',
    'bento/eventsystem',
    'bento/gui/clickbutton',
    'bento/gui/counter',
    'bento/gui/text',
    'bento/utils',
    'bento/tween',
    'components/movable',
    'components/inputhandler'
], function (
    Bento,
    Vector2,
    Rectangle,
    Sprite,
    Clickable,
    Entity,
    EventSystem,
    ClickButton,
    Counter,
    Text,
    Utils,
    Tween,
    Movable,
    InputHandler
) {
    'use strict';
    return function (settings) {
        // --- SHORTHAND --- //
        var viewport = Bento.getViewport();
        var assetPath = 'entities/player/';

        // --- VARIABLES --- // 


        // --- SPRITE --- //
        var sprite = new Sprite({
            imageName: assetPath + 'testImage',
            originRelative: new Vector2(0.5, 0.5)
        });


        // --- INPUT --- //
        var inputHandler = new InputHandler({});


        // --- PLAYER --- //
        var behaviour = {
            name: 'behaviour',
            // PROPERTIES
            // - states 
            onGround: false,
            // - movement
            runAccelMax: 0.25,
            runSpeedMax: 1.5,
            airControl: 1,
            dashMinSpeedY: 2,
            dashMaxSpeedX: 4,
            dashMaxSpeedY: 4,
            airSpeedMax: 5,
            // - friction
            floorFriction: 0.75,
            airFriction: 0.95,
            overlimitFriction: 0.9,
            // - dashing
            dashCount: 1,
            dashCountMax: 1,
            // UPDATE
            update: function (data) {
                // - get references
                if (!inputHandler) {
                    console.error('Missing Input Handler.');
                    return;
                }
                if (!movable) {
                    console.error('Missing Movable.');
                    return;
                }

                // - reset stuff
                var tempAccel = new Vector2(0, 0);
                var tempAccelFactor = 1;
                var tempFriction = new Vector2(1, 1);

                // - states
                this.onGround = (movable.checkSolidRelative(new Vector2(0,1)));

                // - state actions
                if (this.onGround) {
                    tempFriction.x = this.floorFriction;
                    this.dashCount = this.dashCountMax;
                } else {
                    tempFriction.x = this.airFriction;
                    tempAccelFactor = this.airControl;
                }

                // - input actions
                tempAccel.x += inputHandler.joystick.x * this.runAccelMax * tempAccelFactor;
                if (inputHandler.joystick.x !== 0) {
                    tempFriction.x = 1;
                }
                if (!inputHandler.mouseDown) {
                    if (inputHandler.mouseDownLast) {
                        if (inputHandler.swipe.magnitude() > 3) {
                            if (this.dashCount > 0) {
                                if (this.onGround) {
                                    movable.velocity.x += Utils.clamp(-this.dashMaxSpeedX,inputHandler.swipe.x * 0.4,this.dashMaxSpeedX);
                                    movable.velocity.y = Utils.clamp(-this.dashMaxSpeedY, inputHandler.swipe.y * 0.6, -this.dashMinSpeedY);
                                    this.onGround = false;
                                    tempFriction.x = 1;
                                } else {
                                    movable.velocity.x += Utils.clamp(-this.dashMaxSpeedX,inputHandler.swipe.x * 0.4,this.dashMaxSpeedX);
                                    movable.velocity.y += Utils.clamp(-this.dashMaxSpeedY, inputHandler.swipe.y * 0.6, this.dashMaxSpeedY);
                                    this.dashCount--;
                                }
                            }
                        }
                    }
                }

                // - perform
                movable.velocity.addTo(tempAccel);

                // - limit velocity
                var absoluteVelocityX = Math.abs(movable.velocity.x);
                if (absoluteVelocityX > this.runSpeedMax) {
                    movable.velocity.x = Utils.sign(movable.velocity.x) * Math.max(Math.min(absoluteVelocityX * this.overlimitFriction, this.airSpeedMax), this.runSpeedMax);
                }
                var absoluteVelocityY = Math.abs(movable.velocity.y);
                if (absoluteVelocityY > this.airSpeedMax) {
                    movable.velocity.y = Utils.sign(movable.velocity.y) * Math.min(absoluteVelocityY * this.overlimitFriction, this.airSpeedMax);
                }

                // - apply friction
                movable.friction = tempFriction;
            }
        };


        // --- MOVABLE --- //
        var movable = new Movable({
            gravity: 1/8,
            floorFriction: 0.95
        });

        // --- ENTITY --- //
        var entity = new Entity({
            z: Utils.layers.PLAYER,
            name: 'player',
            family: ['players'],
            position: settings.position,
            boundingBox: new Rectangle(-4, -8, 8, 16),
            updateWhenPaused: 0,
            components: [
                sprite,
                inputHandler,
                behaviour,
                movable,
            ]
        });
        return entity;
    };
});