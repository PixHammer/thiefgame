/**
 * Renders an aiming UI when visible from the parameters attached to the entity
 * @moduleName AimingUI
 */
bento.define('components/aimerUI', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/entity',
    'bento/eventsystem',
    'bento/utils',
    'bento/tween'
], function (
    Bento,
    Vector2,
    Rectangle,
    Entity,
    EventSystem,
    Utils,
    Tween
) {
    'use strict';
    return function () {
        var viewport = Bento.getViewport();

        // variables
        var fromPos = new Vector2(0, 0);
        var aimVector = new Vector2(0, 0);
        var dragDist = 0;
        var aimDist = 0;

        //GUI images
        var guiImages = {
        	image: Bento.assets.getImage('GUI/aimingUI'),
        	width: 16,
        	height: 16
        };

        var renderAimer = function (data) {
        	var i;
        	// ARROW
        	// render dotline
            if (aimDist > 0) {
            	for (i = 16; i < aimDist*2; i+=12) {
            		Utils.renderImage(data,guiImages,2,fromPos.add(aimVector.scalarMultiply(i)),0);
            	}
            	// render arrow 
            	Utils.renderImage(data,guiImages,3,fromPos.add(aimVector.scalarMultiply(aimDist*2)),aimVector.angle());
            }

        	// DRAG LINE
        	// render dotline
            if (dragDist > 0) {
            	for (i = 16; i < dragDist; i+=12) {
            		Utils.renderImage(data,guiImages,1,fromPos.add(aimVector.scalarMultiply(-i)),0);
            	}
            	// render end 
            	Utils.renderImage(data,guiImages,0,fromPos.add(aimVector.scalarMultiply(-dragDist)),0);
            }
        };

        var entityUI = new Entity({
            z: Utils.layers.GUI,
            name: 'gui',
            position: new Vector2(0, 0),
            float: true,
            components: [{
	            draw: function (data) {
	            	renderAimer(data);
	            }
            }]
        });

        var entity;
        var component = {
            name: 'aimerUI',
            start: function (data) {
				Bento.objects.attach(entityUI);
            },
            destroy: function (data) {
            	Bento.objects.remove(entityUI);
            },
            attached: function (data) {
                entity = data.entity;
            },
            setup: function (settings) {
            	fromPos = settings.fromPos || fromPos;
            	aimVector = settings.aimVector || aimVector;
            	dragDist = settings.dragDist || dragDist;
            	aimDist = settings.aimDist || aimDist;
            },
            hide: function () {
            	entityUI.visible = false;
            },
            show: function () {
            	entityUI.visible = true;
            },
            toViewPos: function (pos) {
            	return entityUI.toLocalPosition(pos.clone());
            } 
        };
        return component;
    };
});