/**
 * The Player with ball bouncing behaviour
 * @moduleName Player
 */
bento.define('entities/player', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/components/sprite',
    'bento/components/clickable',
    'bento/entity',
    'bento/eventsystem',
    'bento/gui/clickbutton',
    'bento/gui/counter',
    'bento/gui/text',
    'bento/utils',
    'bento/tween',
    'components/aimerUI'
], function (
    Bento,
    Vector2,
    Rectangle,
    Sprite,
    Clickable,
    Entity,
    EventSystem,
    ClickButton,
    Counter,
    Text,
    Utils,
    Tween,
    AimerUI
) {
    'use strict';
    return function (settings) {
        // --- SHORTHAND ---
        var viewport = Bento.getViewport();
        var assetPath = 'entities/player/';

        // --- VARIABLES ---

        //local vars for input
        var mouseDownTicks = 0;
        var mouseIsDown = false;
        var mouseStartedDownOnPlayer = false;
        var mousePos = new Vector2(0, 0);

        // --- SPRITE ---
        var sprite = new Sprite({
            imageName: assetPath + 'testImage',
            originRelative: new Vector2(0.5, 0.5)
        });

        // --- AIMING UI ---
        var aimerUI = new AimerUI({});

        // --- INPUT ---
        var inputBehaviour = new Clickable({
            pointerDown: function (data) {
                mouseIsDown = true;
                if (data.worldPosition.distance(entity.position) < 24) {
                    mouseStartedDownOnPlayer = true;
                    mousePos = data.position.clone();
                }
            },
            pointerUp: function (data) {
                mouseIsDown = false;
                mouseStartedDownOnPlayer = false;
                mousePos = data.position.clone();
            },
            pointerMove: function (data) {
                mousePos = data.position.clone();
            }
        });

        // --- BALL BEHAVIOUR ---
        var ballBehaviour = {
            name: 'ballBehaviour',
            radius: 8,
            gravity: 1/6,
            bounceFactor: 0.5,
            bounceMinimumVelocity: 0.5,
            wallFriction: 0.99,
            velocity: new Vector2(0, 0),
            velocityDecimal: new Vector2(0, 0),
            velocityInteger: new Vector2(0, 0),
            onGround: false,
            update: function (data) {
                // - get references
                var e = this.parent;
                var tc = Bento.objects.get('tileCollider');

                // - states
                this.onGround = (tc.collideCirc(new Vector2(e.position.x, e.position.y+1),this.radius-1));

                // - accelerate
                this.velocity.y += this.gravity;
                // roll off ledges
                if (tc.collidePoint(new Vector2(e.position.x + (this.radius-1),e.position.y + (this.radius-1)))) {
                    this.velocity.x -= this.gravity * 0.5;
                }
                if (tc.collidePoint(new Vector2(e.position.x - (this.radius-1),e.position.y + (this.radius-1)))) {
                    this.velocity.x += this.gravity * 0.5;
                }

                // - move & collide
                // convert decimal velocity to integer
                this.velocityDecimal.addTo(this.velocity);
                this.velocityInteger = new Vector2(Math.round(this.velocityDecimal.x), Math.round(this.velocityDecimal.y));
                this.velocityDecimal.subtractFrom(this.velocityInteger);
                // x
                if (Math.abs(this.velocityInteger.x) !== 0) {
                    //step
                    e.position.x += this.velocityInteger.x;
                    //if we collide move out and stop
                    if (tc.collideCirc(e.position,this.radius-1)) {
                        while (tc.collideCirc(e.position,this.radius-1)) {
                            e.position.x -= Utils.sign(this.velocityInteger.x);
                        }
                        if (Math.abs(this.velocity.x) > this.bounceMinimumVelocity) {
                            this.velocity.x *=-this.bounceFactor;
                        } else {
                            this.velocity.x = 0;
                        }
                    }
                }
                // y
                if (Math.abs(this.velocityInteger.y) !== 0) {
                    // step
                    e.position.y += this.velocityInteger.y;
                    // if we collide move out and stop
                    if (tc.collideCirc(e.position,this.radius-1)) {
                        while (tc.collideCirc(e.position,this.radius-1)) {
                            e.position.y -= Utils.sign(this.velocityInteger.y);
                        }
                        if (Math.abs(this.velocity.y) > this.bounceMinimumVelocity) {
                            this.velocity.y *=-this.bounceFactor;
                        } else {
                            this.velocity.y = 0;
                        }
                    }
                }

                // - friction
                if (this.onGround) {
                    this.velocity.x *= this.wallFriction;
                }

                // - animate
                e.rotation += this.velocity.x/this.radius;
            }
        };

        // --- PLAYER BEHAVIOUR ---
        var playerBehaviour = {
            name: 'playerBehaviour',
            // state vars
            isAiming: false,
            isShooting: false,
            // aiming vars
            canAim: true,
            aimVectorTarget: new Vector2(0, 0),
            aimVector: new Vector2(0, 0),
            aimDragMax: 64,
            aimPowerPercentTarget: 0,
            aimPowerPercent: 0,
            aimPower: 0,
            aimPowerMax: 6,
            // juice
            shootTween: undefined,

            // FUNCTIONS
            start: function (data) {
            },
            destroy: function(data) {
            },
            update: function (data) {
                //temporary vars
                var entityScreenPos = this.parent.position.subtract(new Vector2(viewport.x, viewport.y));

                // do input relevant things such as initiating aiming and braking
                // if we can aim
                if (this.canAim) {
                    // if the mouse is down
                    if (mouseIsDown) {
                        //if mousedown started on the player
                        if (mouseStartedDownOnPlayer) {
                            // aim
                            this.isAiming = true;
                        }
                    } else {
                        // if we were aiming before releasing the mouse
                        if (this.isAiming) {
                            // shoot
                            this.shoot();
                        }
                        this.isAiming = false;
                    }
                } else {
                    // cancel aiming, cancel 'mouseStartedDownOnPlayer' to prevent aiming
                    this.isAiming = false;
                    mouseStartedDownOnPlayer = false;
                }

                // if we're aiming
                if (this.isAiming) {
                    // set targets
                    this.aimVectorTarget = entityScreenPos.subtract(mousePos).normalize();
                    this.aimPowerPercentTarget = Utils.clamp(0,entityScreenPos.distance(mousePos),this.aimDragMax)/this.aimDragMax;
                    // move aim stuff 
                    this.aimVector.addTo((this.aimVectorTarget.subtract(this.aimVector)).scalarMultiply(0.2));
                    this.aimPowerPercent += (this.aimPowerPercentTarget-this.aimPowerPercent) * 0.2;
                    this.aimPower = this.aimPowerPercent * this.aimPowerMax;
                }

                // gui stuff
                if (this.isAiming) {
                    aimerUI.setup({
                        fromPos: aimerUI.toViewPos(entity.position),
                        aimVector: this.aimVector.clone(),
                        dragDist: this.aimPowerPercent * this.aimDragMax
                    });
                    aimerUI.show();
                } else {
                    if (!this.isShooting) {
                        aimerUI.hide();
                    }
                }
            },
            shoot: function () {
                playerBehaviour.canAim = false;
                playerBehaviour.isShooting = true;
                // create the tween for the 'hit' animation
                this.shootTween = new Tween({
                    from: 1,
                    to: 0,
                    in: 20,
                    delay: 0,
                    ease: 'easeInQuad',
                    onStart: function () {
                        // store a copy of the aiming variables on start
                        this.aimVectorBuffer = playerBehaviour.aimVector.clone();
                        this.aimPowerBuffer = playerBehaviour.aimPower;
                        this.aimPowerPercentBuffer = playerBehaviour.aimPowerPercent;
                        this.dragDistBuffer = playerBehaviour.aimPowerPercent * playerBehaviour.aimDragMax;
                    },
                    onUpdate: function (v, t) {
                        // do the UI spring animation
                        aimerUI.setup({
                            fromPos: aimerUI.toViewPos(entity.position),
                            dragDist: this.dragDistBuffer * v
                        });
                    },
                    onComplete: function () {
                        // actually shoot the ball once complete
                        playerBehaviour.isShooting = false;
                        playerBehaviour.canAim = true;
                        ballBehaviour.velocity = this.aimVectorBuffer.scalarMultiply(this.aimPowerBuffer);
                        // reset aiming vars
                        playerBehaviour.aimVectorTarget = new Vector2(0,0);
                        playerBehaviour.aimPowerPercentTarget = 0;
                        playerBehaviour.aimVector = new Vector2(0,0);
                        playerBehaviour.aimPowerPercent = 0;
                        // do a GAME FEEL FREEZE
                        Utils.sleep(100);
                    }
                });
            }
        };

        // --- ENTITY ---
        var entity = new Entity({
            z: Utils.layers.PLAYER,
            name: 'player',
            family: ['players'],
            position: settings.position,
            updateWhenPaused: 0,
            components: [
                sprite,
                aimerUI,
                inputBehaviour,
                ballBehaviour,
                playerBehaviour
            ]
        });
        return entity;
    };
});