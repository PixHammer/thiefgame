/**
 * Extends utils with more useful functions.
 * See bento/utils
 */
bento.define('utils', [
    'bento',
    'bento/math/vector2',
    'bento/math/rectangle',
    'bento/components/sprite',
    'bento/components/clickable',
    'bento/entity',
    'bento/utils',
    'bento/tween'
], function (
    Bento,
    Vector2,
    Rectangle,
    Sprite,
    Clickable,
    Entity,
    Utils,
    Tween
) {
    'use strict';
    var utils = {
        // --- DATA ---
        // layers
        layers: {
            CAMERA: -1,
            BACKGROUND: 0,
            PLAYER: 1,
            GUI: 2
        },
        // --- TIMING ---
        // acts as a timeout 
        timeout: function (time, callback) {
            var tween = new Tween({
                from: 0,
                to: 1,
                in: time,
                ease: 'linear',
                onComplete: callback
            });
            return tween;
        },
        // completely locks the game for X milliseconds (needs mobile testing)
        sleep: function (milliseconds) {
            var endTime = new Date().getTime() + milliseconds;
            while (new Date().getTime() < endTime) {}
        },
        // --- RENDERING ---
        // renders a packed image
        renderImage: function (data,image,frame,atPos,rotation) {
            data.renderer.save();
            data.renderer.translate(atPos.x,atPos.y);
            data.renderer.rotate(rotation);
            data.renderer.drawImage(image.image, image.width*frame, 0, image.width, image.height, -image.width/2, -image.height/2, image.width, image.height);
            data.renderer.restore();
        }
    };
    Utils.extend(Utils, utils);
    return Utils;
});